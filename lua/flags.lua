-- chikun :: 2015
-- Game flags


DEFAULT_SCALE = 0.5
FULLSCREEN    = false
GAME_VSYNC    = true
GAME_WIDTH    = 1920
GAME_HEIGHT   = 1080
GAME_MINWIDTH = 854
GAME_MINHEIGHT = 480

IS_OUYA        = false  -- Whether or not the game is running on an OUYA
IS_PIXEL_BASED = false  -- Whether or not the game is pixel based


local current_dir = ...

-- Load test flags is they exist on the system
if (love.filesystem.exists(current_dir .. "/test_flags.lua")) then

	require(current_dir .. "test_flags")
end
