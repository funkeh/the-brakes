-- chikun :: 2015
-- Control scheme for keyboard

return {
	grow   = { "key_a", "key_left", "key_z", "key_ ", "key_return" },
	rotate = { "key_d", "key_right", "key_x", "key_backspace", "key_lctrl" }
}
