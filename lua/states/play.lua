-- chikun :: 2014-2015
-- Play state


-- Temporary state, removed at end of script
local PlayState = class(PrototypeClass, function(new_class) end)


-- On state create
function PlayState:create()

	self.e_killed = 0
	self.enemies = { }
	self.lines  = { }
	self.points = { }
	self.score  = 0
	self.theme  = math.random(1, 5)
	self.end_timer = 1
	self.start_timer = 3
	self.timer = 90
	self.triangle_count = 0

	sfx.door:play()

	cx.play(cb["bg" .. self.theme], 0.8)

	-- Generate points in game
	for y = 0, 8 do

		self.points[y + 1] = { }

		local y_table = self.points[y + 1]

		if (y % 2 == 0) then

			for x = 0, 20, 2 do y_table[x + 1] = { } end
		else

			for x = 1, 19, 2 do y_table[x + 1] = { } end
		end
	end

	-- Claim first point
	self.points[5][11].claimed = true

	self.current_point = {
		dir = 1,
		x   = 11,
		y   = 5,
		last_x = 1,
		last_y = 1,
		trans  = 0
	}

	alphas = {
		64,
		32,
		0
	}

	self.bgs = {
		gfx.bg1,
		gfx.bg2,
		gfx.bg3,
		gfx.bg4,
		gfx.bg5
	}

	self.cols = {
		{ 4, 255, 150 },
		{ 255, 150, 4 },
		{ 150, 4, 255 },
		{ 4, 155, 255 },
		{ 255, 4, 109 }
	}

	-- Possible directions for rotation marker to point in
	self.dirs = { 0, 45, 135, 180, 225, 315 }
end


-- On state update
function PlayState:update(dt)

	if (self.start_timer > 0) then

		local old = self.start_timer

		self.start_timer = math.max(0, self.start_timer - dt)

		if (old == 3) then
			sfx.three:play()
		elseif (old > 2 and self.start_timer <= 2) then
			sfx.two:play()
		elseif (old > 1 and self.start_timer <= 1) then
			sfx.one:play()
		elseif (self.start_timer == 0) then
			sfx.go:play()
		end

		return 0
	end

	local old = self.timer
	self.timer = math.max(0, self.timer - dt)


	if (old > 60 and self.timer <= 60) then

		sfx.enemy_spawn:play()
		sfx.enemies:play()

		local to_spawn = math.ceil((self.triangle_count + 1) / 14)

		for i = 1, to_spawn do

			self:enemyCreate()
		end

	elseif (old > 30 and self.timer <= 30) then

		sfx.enemy_spawn:play()
		sfx.watch_out:play()

		local to_spawn = math.ceil((self.triangle_count + 1) / 10)

		for i = 1, to_spawn do

			self:enemyCreate()
		end

	elseif (old > 10 and self.timer <= 10) then

		sfx.ten:play()
	elseif (old > 3 and self.timer <= 3) then

		sfx.three:play()
	elseif (old > 2 and self.timer <= 2) then

		sfx.two:play()
	elseif (old > 1 and self.timer <= 1) then

		sfx.one:play()
	elseif (self.timer == 0) then

		if (old > 0) then

			if (self.score >= 5000) then

				sfx.yeah_buddy:play()
			else

				sfx.time:play()
			end
			sfx.door:play()
		end

		self.end_timer = math.max(0, self.end_timer - dt)
		if (self.end_timer == 0) then

			score = self.score
			tris  = self.triangle_count
			cs:change("score")
		end
		return 0
	end

	local point = self.current_point

	self:enemyUpdate(dt)

	if (not point.dir_anim) then

		if (love.system.getOS() == "Android" and lm.isDown('l')) then

			if (lm.getX() < lw.getWidth() / 2) then

				self:rotate_left()
			else

				self:rotate_right()
			end
		elseif (ci:isDown('rotate')) then
			self:rotate_right()
		elseif (ci:isDown('grow')) then
			self:rotate_left()
		end
	end

	for key, line in ipairs(self.lines) do

		line.timer = math.max(0, line.timer - dt)
	end

	if (point.trans == 0) then

		self:grow()
	else

		point.trans = math.max(0, point.trans - dt * point.speed)
	end

	-- Animate rotation of direction dot
	if (point.dir_anim) then

		-- Increate rotation time
		point.dir_anim.time = point.dir_anim.time + dt * 10

		-- End animation if timer expires
		if (point.dir_anim.time >= 1) then point.dir_anim = nil end
	end

	-- Animate movement of dot
	if (point.dot_anim) then

		-- Increate rotation time
		point.dot_anim.time = point.dot_anim.time + dt * 40

		-- End animation if timer expires
		if (point.dot_anim.time >= 1) then point.dot_anim = nil end
	end

	for key, alpha in ipairs(alphas) do

		alphas[key] = math.clamp(0, alpha + (math.random() * 64 - 32) * dt, 64)
	end
end


-- On state draw
function PlayState:draw()

	-- Reset colour values
	lg.setColor(255, 255, 255)

	-- Draw background image
	lg.draw(self.bgs[self.theme], 0, 0)

	if (not love.system.getOS() == "Android") then

		-- Draw space background stuff
		lg.setColor(255, 255, 255, alphas[1])
		lg.draw(gfx.over1, 0, 0)
		lg.setColor(255, 255, 255, alphas[2])
		lg.draw(gfx.over2, 0, 0)
		lg.setColor(255, 255, 255, alphas[3])
		lg.draw(gfx.over3, 0, 0)
	end

	local point = self.current_point
	local dist_x, dist_y = 110, 140
	local calc = {
		x = point.x,
		y = point.y
	}
	if (point.trans > 0) then

		calc.x = calc.x + (point.last_x - calc.x) * point.trans
		calc.y = calc.y + (point.last_y - calc.y) * point.trans
	end
	local p_x, p_y =
		(calc.x - 1),
		(calc.y - 1)
	local dot_x, dot_y =
		p_x * dist_x,
		p_y * dist_y
	local x_offset, y_offset =
		960 - dot_x,
		540 - dot_y

	x_offset = (x_offset + 140) * 0.3 - 140
	y_offset = (y_offset + 20)  * 0.3 - 20

	-- Set colour for triangle
	local col = self.cols[self.theme]
	lg.setColor(col[1], col[2], col[3], 192)

	-- Draw all triangles and count number drawn
	--self.triangle_count
	self:drawTriangles(x_offset, y_offset, dist_x, dist_y)

	-- Reset colour values
	lg.setColor(255, 255, 255)

	-- Increase thickness for lines
	lg.setLineWidth(6)

	-- Iterate through all lines
	for key, line in ipairs(self.lines) do

		-- Combine line points into a table
		local line = {
			x_offset + (line[1].x - 1) * dist_x,
			y_offset + (line[1].y - 1) * dist_y,
			x_offset + (line[2].x - 1) * dist_x,
			y_offset + (line[2].y - 1) * dist_y
		}

		-- Draw the line
		lg.line(line)
	end

	local rect = {
		x = x_offset - dist_x,
		y = y_offset - dist_x,
		w = dist_x * 22,
		h = dist_y * 8 + dist_x * 2
	}

	lg.setLineWidth(math.random(6, 12))
	lg.setColor(0, 0, 0)
	lg.rectangle('line', rect.x, rect.y, rect.w, rect.h)
	lg.setLineWidth(3)
	lg.setColor(col[1], col[2], col[3])
	lg.rectangle('line', rect.x + 3, rect.y + 3, rect.w - 6, rect.h - 6)
	lg.setColor(255, 255, 255)

	-- Reset line thickness
	lg.setLineWidth(1)

	lg.setColor(255, 255, 255)

	-- Draw all dots
	for y = 0, 8 do

		if (y % 2 == 0) then

			for x = 0, 20, 2 do

				local dot = self.points[y + 1][x + 1]
				self:drawDot(x_offset + x * dist_x, y_offset + y * dist_y, dot)
			end
		else

			for x = 1, 19, 2 do

				local dot = self.points[y + 1][x + 1]
				self:drawDot(x_offset + x * dist_x, y_offset + y * dist_y, dot)
			end
		end
	end

	-- Calculate position and direction of pointer
	local x, y, dir = x_offset + dot_x, y_offset + dot_y, self.dirs[point.dir]

	-- If pointer is animating, calculate in-between
	if (point.dir_anim) then

		dir = dir - (dir - point.dir_anim.old_dir) * (1 - point.dir_anim.time)
	end

	cg.drawCentred(gfx.pointer, x, y, math.rad(dir))

	-- SCORES

	lg.setFont(cf.small)
	for key, line in ipairs(self.lines) do

		if (line.timer > 0) then

			lg.setColor(237, 232, 7, line.timer * 255)
			local lin = {
				x_offset + (line[1].x - 1) * dist_x,
				y_offset + (line[1].y - 1) * dist_y,
				x_offset + (line[2].x - 1) * dist_x,
				y_offset + (line[2].y - 1) * dist_y
			}
			lg.printf("+10", (lin[1] + lin[3]) / 2,
			                 (lin[2] + lin[4]) / 2 - 96 + 32 * line.timer,
			                 0, 'center')
		end
	end

	-- GUI

	lg.setFont(cf.small_full)

	local timer = math.ceil(self.timer)
	local time_str = string.format("%1d:%02d", math.floor(timer / 60),
	                               math.floor(timer % 60))

	lg.setFont(cf.time)
	lg.setColor(0, 0, 0)
	lg.print(time_str, 42, 26)
	lg.setColor(255, 255, 255)
	lg.print(time_str, 40, 24)

	local score = string.format("%05d", self.score)

	for i = 1, 5 do

		lg.setColor(0, 0, 0)
		lg.printf(score:sub(i, i), 1572 + (i - 1) * 64, 26, 64, 'center')
		lg.setColor(255, 255, 255)
		lg.printf(score:sub(i, i), 1570 + (i - 1) * 64, 24, 64, 'center')
	end

	local f_message, f_fade = "", 1

	lg.setFont(cf.full)

	if (sfx.first_blood:isPlaying()) then

		lg.setFont(cf.small_full)
		f_message = "FIRST BLOOD!"
		f_fade = math.max(0, 1 - sfx.first_blood:tell('seconds'))
	elseif (self.timer > 59 and self.timer <= 60) then

		lg.setFont(cf.small_full)
		f_message = "ENEMIES!"
		f_fade = self.timer - 59
	elseif (self.timer > 29 and self.timer <= 30) then

		lg.setFont(cf.small_full)
		f_message = "WATCH OUT!"
		f_fade = self.timer - 29
	elseif (self.start_timer > 0) then

		f_message = math.ceil(self.start_timer)
		f_fade = (self.start_timer - 0.00001) % 1

	elseif (self.timer > 89) then

		f_message = "GO!"
		f_fade = self.timer - 89
	elseif (self.timer > 9 and self.timer <= 10) then

		lg.setFont(cf.small_full)
		f_message = "10 SECONDS LEFT!"
		f_fade = self.timer - 9
	elseif (self.timer <= 3 and self.timer > 0) then

		f_message = math.ceil(self.timer)
		f_fade = (self.timer - 0.00001) % 1
	elseif (self.timer == 0) then

		f_message = "TIME!"
		f_fade = self.end_timer

		if (self.score >= 5000) then

			lg.setFont(cf.small_full)
			f_message = "YEAH BUDDY!"
		end
	end

	local y = 540 - lg.getFont():getHeight() / 2
	lg.setColor(0, 0, 0, math.max(0, f_fade * 2 - 1) * 255)
	lg.printf(f_message, 4, y + 4, 1920, 'center')
	lg.setColor(255, 255, 255, f_fade * 255)
	lg.printf(f_message, 0, y, 1920, 'center')

	if (self.start_timer > 2 or self.timer == 0) then

		local offset = (1 - (self.start_timer - 2)) * 540

		if (self.timer == 0) then

			offset = (self.end_timer * 540)
		end

		lg.setColor(255, 255, 255)
		lg.draw(gfx.door, 0, -offset)
		lg.draw(gfx.door, 0, 540 + offset)
	end
end


function PlayState:click(click)

	if (click.x >= 40 and click.x < 240 and
	    click.y > 840 and click.y < 1040) then

		self:rotate_left()
	elseif (click.x >= 280 and click.x < 480 and
	        click.y > 840 and click.y < 1040) then

		self:rotate_right()
	end
end


-- Performed upon rotation of marker
function PlayState:rotate_left()

	local point = self.current_point

	point.dir_anim = {
		time = 0,
		old_dir = self.dirs[point.dir],
		dir = -1
	}
	point.dir = point.dir - 1

	if (point.dir == 0) then

		point.dir = 6
	end

	if (point.dir == 6) then

		point.dir_anim.old_dir = point.dir_anim.old_dir + 360
	end
end


-- Performed upon rotation of marker
function PlayState:rotate_right()

	local point = self.current_point

	point.dir_anim = {
		time = 0,
		old_dir = self.dirs[point.dir],
		dir = 1
	}
	point.dir = (point.dir % 6) + 1

	if (point.dir == 1) then

		point.dir_anim.old_dir = point.dir_anim.old_dir - 360
	end
end


-- Performed upon growth of graph
function PlayState:grow()

	local point = self.current_point

	-- Start next point at current point
	local np = {
		x = point.x,
		y = point.y
	}

	-- Determine next position based on point direction
	if (point.dir == 1) then
		np.x = np.x + 2
	elseif (point.dir == 2) then
		np.x = np.x + 1 ; np.y = np.y + 1
	elseif (point.dir == 3) then
		np.x = np.x - 1 ; np.y = np.y + 1
	elseif (point.dir == 4) then
		np.x = np.x - 2
	elseif (point.dir == 5) then
		np.x = np.x - 1 ; np.y = np.y - 1
	elseif (point.dir == 6) then
		np.x = np.x + 1 ; np.y = np.y - 1
	end

	local next_point, yes = self.points[np.y], false

	if (next_point) then

		next_point = next_point[np.x]

		if (next_point) then

			yes = true

			if (not self:lineExists(point, np)) then

				self.score = self.score + 10
				self.current_point.speed  = 3
				self:addLine(point, np)
				cx.play(sfx.line, 0.4, 1.1 + math.random() / 10)
			else

				cx.play(sfx.move, 0.9, 1.1 + math.random() / 10)
			end

			self.current_point.speed  = 6

			if (not next_point.claimed) then

				next_point.claimed = true
			end

			self.current_point.trans  = 1
			self.current_point.last_x = point.x
			self.current_point.last_y = point.y
			self.current_point.x = np.x ; self.current_point.y = np.y
		end
	end

	if (not yes) then

		point.dir = (point.dir + 1) % 6 + 1
		self:grow()
	end
end


-- Add line to list
function PlayState:addLine(p1, p2)

	-- Swap points if required
	if (p2.y < p1.y or (p1.y == p2.y and p2.x < p1.x)) then

		local old_p1, old_p2 = p1, p2
		p1 = old_p2
		p2 = old_p1
	end

	-- Insert the new line
	table.insert(self.lines, {
			{ x = p1.x, y = p1.y },
			{ x = p2.x, y = p2.y },
			timer = 1
		})
end


-- Will return whether a line exists between two points
function PlayState:lineExists(p1, p2)

	-- Swap points if required
	if (p2.y < p1.y or (p1.y == p2.y and p2.x < p1.x)) then

		local old_p1, old_p2 = p1, p2
		p1 = old_p2
		p2 = old_p1
	end

	-- Iterate through all lines
	for key, line in ipairs(self.lines) do

		-- Return true if line exists
		if (line[1].x == p1.x and line[1].y == p1.y and
		    line[2].x == p2.x and line[2].y == p2.y) then

			return line.timer
		end
	end

	-- Else, return false
	return false
end


-- Draw a dot
function PlayState:drawDot(x, y, dot)

	-- Default radius
	local radius = 8

	-- Increase radius if dot is claimed
	if (dot.claimed) then radius = 24 end

	lg.setColor(255, 255, 255)

	if (dot.taken) then

		lg.setColor(0, 0, 0, math.min(dot.timer, 1) * 255)
		local timer = (5 - dot.timer) / 5
		lg.circle('fill', x, y, 40 + timer * 8, 4)
		radius = 32
		lg.setColor(225, 15, 15)
	end

	-- Actually draw the dot
	lg.circle('fill', x, y, radius, 4)

	if (dot.taken) then

		lg.setFont(cf.small)
		lg.setColor(0, 0, 0)
		lg.printf(math.ceil(dot.timer), x - 13, y - 14, 30, 'center')
		lg.setColor(255, 255, 255)
		lg.printf(math.ceil(dot.timer), x - 15, y - 16, 30, 'center')
	end
end


-- Draw the triangles
function PlayState:drawTriangles(x_offset, y_offset, dist_x, dist_y)

	local count = 0

	for y = 1, 8 do

		if (y % 2 == 1) then

			for x = 1, 19, 2 do

				count = count + self:drawTriTop(x, y, x_offset, y_offset, dist_x, dist_y)
			end

			for x = 2, 18, 2 do

				count = count + self:drawTriBottom(x, y + 1, x_offset, y_offset, dist_x, dist_y)
			end
		else

			for x = 2, 18, 2 do

				count = count + self:drawTriTop(x, y, x_offset, y_offset, dist_x, dist_y)
			end

			for x = 1, 19, 2 do

				count = count + self:drawTriBottom(x, y + 1, x_offset, y_offset, dist_x, dist_y)
			end
		end
	end

	return count
end


function PlayState:drawTriBottom(x, y, x_offset, y_offset, dist_x, dist_y)

	local count = 0

	if (self.points[y][x].claimed and
		self.points[y][x + 2].claimed and
		self.points[y - 1][x + 1].claimed) then

		local point_a, point_b, point_c =
			{ x = x, y = y },
			{ x = x + 2, y = y },
			{ x = x + 1, y = y - 1 }

		if (self:lineExists(point_a, point_b) and
			self:lineExists(point_a, point_c) and
			self:lineExists(point_b, point_c)) then

			local timer = self:lineExists(point_a, point_b)
			timer = math.max(timer, self:lineExists(point_a, point_c))
			timer = math.max(timer, self:lineExists(point_b, point_c))

			local verts = {
				x_offset + (point_a.x - 1) * dist_x,
				y_offset + (point_a.y - 1) * dist_y,
				x_offset + (point_b.x - 1) * dist_x,
				y_offset + (point_b.y - 1) * dist_y,
				x_offset + (point_c.x - 1) * dist_x,
				y_offset + (point_c.y - 1) * dist_y
			}

			lg.polygon('fill', verts)

			count = count + 1

			if (timer > 0) then

				local col = {lg.getColor()}
				lg.setColor(255, 255, 255, timer * 255)
				lg.setFont(cf.small)
				lg.printf("+30", (verts[1] + verts[3] + verts[5]) / 3,
								 (verts[2] + verts[4] + verts[6]) / 3 - 20,
								 0, 'center')
				if (timer == 1) then

					self.score = self.score + 30
					cx.play(sfx.tri, 0.6, 1.1 + math.random() / 10)
					self.triangle_count = self.triangle_count + 1
				end
				lg.setColor(col[1], col[2], col[3], col[4])
			end
		end
	end

	return count
end

function PlayState:drawTriTop(x, y, x_offset, y_offset, dist_x, dist_y)

	local count = 0

	if (self.points[y][x].claimed and
		self.points[y][x + 2].claimed and
		self.points[y + 1][x + 1].claimed) then

		local point_a, point_b, point_c =
			{ x = x, y = y },
			{ x = x + 2, y = y },
			{ x = x + 1, y = y + 1 }

		if (self:lineExists(point_a, point_b) and
			self:lineExists(point_a, point_c) and
			self:lineExists(point_b, point_c)) then

			local timer = self:lineExists(point_a, point_b)
			timer = math.max(timer, self:lineExists(point_a, point_c))
			timer = math.max(timer, self:lineExists(point_b, point_c))

			local verts = {
				x_offset + (point_a.x - 1) * dist_x,
				y_offset + (point_a.y - 1) * dist_y,
				x_offset + (point_b.x - 1) * dist_x,
				y_offset + (point_b.y - 1) * dist_y,
				x_offset + (point_c.x - 1) * dist_x,
				y_offset + (point_c.y - 1) * dist_y
			}

			lg.polygon('fill', verts)

			count = count + 1

			if (timer > 0) then

				local col = {lg.getColor()}
				lg.setColor(255, 255, 255, timer * 255)
				lg.setFont(cf.small)
				lg.printf("+30", (verts[1] + verts[3] + verts[5]) / 3,
								 (verts[2] + verts[4] + verts[6]) / 3 - 20,
								 0, 'center')
				if (timer == 1) then

					self.score = self.score + 30
					cx.play(sfx.tri, 0.6, 1.1 + math.random() / 10)
					self.triangle_count = self.triangle_count + 1
				end
				lg.setColor(col[1], col[2], col[3], col[4])
			end
		end
	end

	return count
end


-- On state kill
function PlayState:kill() end


-- Add line to list
function PlayState:removeLines(x, y)

	local linesToRemove = { }

	for key, line in ipairs(self.lines) do

		if ((x == line[1].x and y == line[1].y) or
		    (x == line[2].x and y == line[2].y)) then

			table.insert(linesToRemove, key)
		end
	end

	for i = #linesToRemove, 1, -1 do

		table.remove(self.lines, linesToRemove[i])
	end
end


function PlayState:enemyUpdate(dt)

	for key, enemy in ipairs(self.enemies) do

		enemy.timer = math.max(0, enemy.timer - dt)

		self.points[enemy.y][enemy.x].timer = enemy.timer

		if (enemy.x == self.current_point.x and
		    enemy.y == self.current_point.y) then

			self.points[enemy.y][enemy.x].taken = nil

			self.e_killed = self.e_killed + 1
			sfx.enemy_killed:play()

			self.score = self.score + 50

			if (self.e_killed == 1) then

				sfx.first_blood:play()
			end

			table.remove(self.enemies, key)
		elseif (enemy.timer == 0) then

			self.points[enemy.y][enemy.x].claimed = false
			self.points[enemy.y][enemy.x].taken = nil
			self:removeLines(enemy.x, enemy.y)
			sfx.enemy_die:play()
			sfx.node_down:play()
			self.score = self.score - 200

			table.remove(self.enemies, key)
		end
	end
end

function PlayState:enemyCreate()

	local allowed_points = { }

	for y = 1, 9 do

		if (y % 2 == 1) then

			for x = 1, 21, 2 do

				local point = self.points[y][x]

				if (point.claimed and not point.taken and not (x == self.current_point.x and
					y == self.current_point.y)) then

					table.insert(allowed_points, { x = x, y = y })
				end
			end
		else

			for x = 2, 20, 2 do

				local point = self.points[y][x]

				if (point.claimed and not point.taken and not (x == self.current_point.x and
					y == self.current_point.y)) then

					table.insert(allowed_points, { x = x, y = y })
				end
			end
		end
	end

	if (#allowed_points > 1) then

		local num = math.random(1, #allowed_points)

		local p = allowed_points[num]

		self.points[p.y][p.x].taken = true

		table.insert(self.enemies, { x = p.x, y = p.y, timer = 10 })
	end
end




-- Transfer data to state loading script
return PlayState
