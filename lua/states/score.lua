-- chikun :: 2014-2015
-- Score state


-- Temporary state, removed at end of script
local ScoreState = class(PrototypeClass, function(new_class) end)


-- On state create
function ScoreState:create()

	lg.setBackgroundColor(0, 0, 0)

	cb.intro:play()

	self.text = "You scored " .. score .. " points!\nYou also built " .. tris .. " triangles."
end


-- On state update
function ScoreState:update(dt)

	if (lm.isDown(1)) then

		cs:change("play")
	end
end


-- On state draw
function ScoreState:draw()

	lg.setColor(255, 255, 255)
	lg.draw(gfx.door, 0, 0)
	lg.draw(gfx.door, 0, 540)

	lg.setFont(cf.score)
	lg.setColor(0, 0, 0)
	lg.printf(self.text, 4, 124, 1920, 'center')
	lg.setColor(255, 255, 255)
	lg.printf(self.text, 0, 120, 1920, 'center')

	lg.setFont(cf.score)
	lg.setColor(0, 0, 0)
	lg.printf("Click or touch to play again!", 4, 724, 1920, 'center')
	lg.setColor(255, 255, 255)
	lg.printf("Click or touch to play again!", 0, 720, 1920, 'center')
end


-- On state kill
function ScoreState:kill()

	cb.intro:stop()
end


-- Transfer data to state loading script
return ScoreState
