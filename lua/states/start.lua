-- chikun :: 2014-2015
-- Start state


-- Temporary state, removed at end of script
local StartState = class(PrototypeClass, function(new_class) end)


-- On state create
function StartState:create()

	lg.setBackgroundColor(0, 0, 0)

	cb.intro:play()

	self.intro_text = [[
	You have to build a triangle starfield, but you have no brakes!
	Use Z and X, A and D, Left and Right, or touch the sides of the screen to steer your ship.
	Run into enemies or they will explode parts of your starfield!
	Click or touch to start.
	]]
end


-- On state update
function StartState:update(dt)

	if (lm.isDown(1)) then

		cs:change("play")
	end
end


-- On state draw
function StartState:draw()

	lg.setColor(255, 255, 255)
	lg.draw(gfx.door, 0, 0)
	lg.draw(gfx.door, 0, 540)

	lg.setFont(cf.full)
	lg.setColor(0, 0, 0)
	lg.printf("THE BRAKES!!!", 4, 64, 1920, 'center')
	lg.setColor(255, 255, 255)
	lg.printf("THE BRAKES!!!", 0, 60, 1920, 'center')

	lg.setFont(cf.intro)
	lg.setColor(0, 0, 0)
	lg.printf(self.intro_text, 4, 684, 1920, 'center')
	lg.setColor(255, 255, 255)
	lg.printf(self.intro_text, 0, 680, 1920, 'center')
end


-- On state kill
function StartState:kill()

	cb.intro:stop()
end


-- Transfer data to state loading script
return StartState
