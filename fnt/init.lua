-- chikun :: 3015
-- Load all fonts


local fnt = {

	splash = love.graphics.newFont(... .. "/exo2.otf", 240),
	score = love.graphics.newFont(... .. "/timeburnernormal.ttf", 120),
	small_full = love.graphics.newFont(... .. "/timeburnernormal.ttf", 240),
	full = love.graphics.newFont(... .. "/timeburnernormal.ttf", 320),
	time = love.graphics.newFont(... .. "/timeburnernormal.ttf", 100),
	intro = love.graphics.newFont(... .. "/timeburnernormal.ttf", 48),
	small = love.graphics.newFont(... .. "/timeburnernormal.ttf", 32)
}

return fnt
