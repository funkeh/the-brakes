-- chikun :: 2015
-- Game core


-- If the game is pixel based, set nearest filter
if (IS_PIXEL_BASED) then love.graphics.setDefaultFilter('nearest') end

-- Load in all libraries
require("lua")


--[[
	Run upon game load.
]]
function love.load()

	local width, height = love.window.getDesktopDimensions(1)

	GAME_SCALE = 1

	-- Modify game size for Android devices
	if (love.system.getOS() == "Android") then

		GAME_WIDTH = height / 9 * 16
		GAME_HEIGHT = height
		GAME_SCALE = height / 1080
	end

	-- Scale new canvas and get scale values
	game_canvas = lg.newCanvas(GAME_WIDTH, GAME_HEIGHT)
	off_x, off_y, scale = getScale(lg.getWidth(), lg.getHeight())

	-- Load original state
	cs:load("splash")
end


--[[
	Run upon game update.
	INPUT:  Delta time.
]]
function love.update(dt)

	-- Update input controller
	ci:update()

	-- Test input controller
	local axis = ci:getValue("right") - ci:getValue("left")

	-- Induce lag if game drops below 15 FPS
	dt = math.min(dt, 1 / 15)

	-- Update state
	cs:update(dt)

	-- Update sound effects
	cx.update()
end


--[[
	Run upon game draw.
]]
function love.draw()

	-- Draw current state to game canvas after clearing it
	lg.setCanvas(game_canvas)
		lg.clear()
		lg.scale(GAME_SCALE)
		cs:draw()
	lg.setCanvas()
	lg.scale(1 / GAME_SCALE)

	-- Actually draw game_canvas
	lg.setColor(255, 255, 255)
	lg.draw(game_canvas, off_x, off_y, 0, scale)
end


--[[
	Run upon game resize.
	INPUT:  width and height are the dimensions of the resized window.
]]
function love.resize(width, height)

	-- Recalculate canvas scaling
	off_x, off_y, scale, max_scale = getScale(width, height)
end


function getClick()

	return {
		x = (lm.getX() - off_x) / scale,
		y = (lm.getY() - off_y) / scale,
		w = 0,
		h = 0
	}
end


--[[
	Returns details for canvas scaling.
	INPUT:  width and height are the dimensions of the game window.
	OUTPUT: off_x and off_y are offset parameters for canvas x and y.
	        scale is the scaling value for the canvas.
]]
function getScale(width, height)

	-- Calculate horizontal and vertical scale values
	local scale_w, scale_h = (width / GAME_WIDTH), (height / GAME_HEIGHT)

	-- Calculate offsets based on scale values
	local off_x, off_y = math.max((scale_w - scale_h) * GAME_WIDTH / 2, 0),
	                     math.max((scale_h - scale_w) * GAME_HEIGHT / 2, 0)

	-- Return calculated offsets and the minimum scale value
	return off_x, off_y, math.min(scale_w, scale_h), math.max(scale_w, scale_h)
end
